# angular-state-management-demo

A Nx mono-repo containing a fake backend and two Angular applications using two different libraries for state management, namely NGXS and Akita. 

## Getting started

##### Applications

The applications that are included in this repo are:

*flight-api*

Fake backend built with the Nest framework.

*flight-app*

Angular application using Akita for state management.

*flight-app-ngxs*
    
Angular application using NGXS for state management.

##### Libraries

All three applications are using a shared library for models:

*data*

Library for shared data models.

## Installation

Run `npm install` to install the dependencies.

## Development

Run `npm run start:dev` to start all applications at once.

This will serve the flight-api, flight-app, flight-app-ngxs applications on `localhost:3333`, `localhost:4200` and `localhost:4300`, respectively.
