import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { FlightModule } from './features/flight/flight.module';
import { FlightState } from './state/flight.state';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxsModule.forRoot([
      FlightState
    ], { developmentMode: !environment.production }),
    NgxsRouterPluginModule.forRoot(),
    environment.production ? [] : NgxsReduxDevtoolsPluginModule.forRoot(),

    FlightModule,

    RouterModule.forRoot([{ path: '', redirectTo: 'flights', pathMatch: 'full' }])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
