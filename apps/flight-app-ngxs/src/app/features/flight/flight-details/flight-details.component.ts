import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Flight } from '@nx-flights/data';
import { Observable } from 'rxjs';
import { DelayFlight, DeleteFlight, GetFlight } from '../../../actions/flight.actions';
import { FlightQuery } from '../../../state/flight.query';

@Component({
  selector: 'nx-flight-details',
  templateUrl: './flight-details.component.html'
})
export class FlightDetailsComponent implements OnInit {

  flightId = this.store.selectSnapshot<string>((state: any) => state.router.state.root.firstChild.params.flightId);

  @Select(FlightQuery.getSelectedFlight)
  flight$: Observable<Flight>;

  constructor(private store: Store, private location: Location) {}

  ngOnInit(): void {
    const hasFlight = this.store.selectSnapshot((state: any) => state.flights.flights.find(flight => flight.id === this.flightId));

    if (!hasFlight) {
      this.store.dispatch(new GetFlight(this.flightId));
    }
  }

  delayFlight(): void {
    this.store.dispatch(new DelayFlight(this.flightId));
    this.location.back();
  }

  deleteFlight(): void {
    this.store.dispatch(new DeleteFlight(this.flightId));
    this.location.back();
  }

}
