import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { FlightListComponent } from './flight-list/flight-list.component';
import { FlightDetailsComponent } from './flight-details/flight-details.component';
import { FLIGHT_ROUTES } from './flight.routes';

@NgModule({
  declarations: [
    FlightListComponent,
    FlightDetailsComponent
  ],
  exports: [
    FlightListComponent,
    FlightDetailsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,

    MatButtonModule,

    RouterModule.forChild(FLIGHT_ROUTES)
  ]
})
export class FlightModule {}
