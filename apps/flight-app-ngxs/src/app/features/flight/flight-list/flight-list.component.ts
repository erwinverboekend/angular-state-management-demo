import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Flight } from '@nx-flights/data';
import { Observable } from 'rxjs';
import { GetFlights } from '../../../actions/flight.actions';
import { FlightState } from '../../../state/flight.state';

@Component({
  selector: 'nx-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.scss']
})
export class FlightListComponent implements OnInit, OnDestroy {

  @Select(FlightState.isLoading)
  flightsLoading$: Observable<boolean>;

  @Select(FlightState.getCount)
  flightCount$: Observable<number>;

  @Select(FlightState.getFlights)
  flights$: Observable<Flight[]>;

  constructor(private store: Store) {
  }

  ngOnInit(): void {
    const hasCache = this.store.selectSnapshot((state: any) => state.flights.cache);

    if (!hasCache) {
      this.store.dispatch(new GetFlights());
    }
  }

  ngOnDestroy(): void {
  }

}
