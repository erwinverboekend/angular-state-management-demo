import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { Flight } from '@nx-flights/data';
import { tap } from 'rxjs/operators';
import { DelayFlight, DeleteFlight, GetFlight, GetFlights } from '../actions/flight.actions';
import { FlightService } from '../services/flight.service';

export class FlightStateModel {
  flights: Flight[];
  cache: boolean;
  loading: boolean;
}

@State<FlightStateModel>({
  name: 'flights',
  defaults: {
    flights: [],
    cache: false,
    loading: false
  }
})
@Injectable()
export class FlightState {

  constructor(private flightService: FlightService) {
  }

  @Selector()
  static isLoading(state: FlightStateModel) {
    return state.loading;
  }

  @Selector()
  static getCount(state: FlightStateModel) {
    return state.flights.length;
  }

  @Selector()
  static getFlights(state: FlightStateModel) {
    return state.flights;
  }

  @Action(GetFlights)
  getFlights({ patchState }: StateContext<FlightStateModel>, { }: GetFlights) {
    patchState({ loading: true });
    return this.flightService.loadFlights().pipe(tap((flights: Flight[]) => {
      patchState({
        loading: false,
        flights: flights,
        cache: true
      });
    }));
  }

  @Action(GetFlight)
  getFlight({ getState, patchState }: StateContext<FlightStateModel>, { payload }: GetFlight) {
    return this.flightService.loadFlight(payload).pipe(tap((flight: Flight) => {
      const state = getState();
      patchState({
        flights: [...state.flights, flight]
      })
    }));
  }

  @Action(DelayFlight)
  delayFlight({ getState, setState }: StateContext<FlightStateModel>, { payload }: DelayFlight) {
    return this.flightService.delayFlight(payload).subscribe(() => {
      setState(
        patch({
          flights: updateItem<Flight>(flight => flight.id === payload, patch<Flight>({ delayed: true }))
        })
      )
    });
  }

  @Action(DeleteFlight)
  deleteFlight({ getState, patchState }: StateContext<FlightStateModel>, { payload }: DeleteFlight) {
    return this.flightService.deleteFlight(payload).subscribe(() => {
      const state = getState();
      patchState({
        flights: state.flights.filter(flight => flight.id !== payload)
      });
    });
  }

}
