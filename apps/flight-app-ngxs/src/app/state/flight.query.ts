import { RouterState, RouterStateModel } from '@ngxs/router-plugin';
import { Selector } from '@ngxs/store';
import { FlightState, FlightStateModel } from './flight.state';

export class FlightQuery {

  @Selector([FlightState, RouterState])
  static getSelectedFlight(flightState: FlightStateModel, routerState: RouterStateModel) {
    const flightId = routerState.state.root.firstChild.params.flightId;
    return flightState.flights.find(flight => flight.id === flightId);
  }

}
