import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Flight } from '@nx-flights/data';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FlightService {

  baseUrl = '/api/flights';

  constructor(private http: HttpClient) {
  }

  loadFlight(id: string): Observable<Flight> {
    console.info('Load flight');

    const endpoint = `${this.baseUrl}/${id}`;
    return this.http.get<Flight>(endpoint);
  }

  loadFlights(): Observable<Flight[]> {
    console.info('Load flights');

    const endpoint = `${this.baseUrl}`;
    return this.http.get<Flight[]>(endpoint);
  }

  delayFlight(id: string): Observable<void> {
    console.info('Delay flight');

    const endpoint = `${this.baseUrl}/${id}/delay`;
    return this.http.post<void>(endpoint, null);
  }

  deleteFlight(id: string): Observable<void> {
    console.info('Delete flight');

    const endpoint = `${this.baseUrl}/${id}`;
    return this.http.delete<void>(endpoint);
  }

}
