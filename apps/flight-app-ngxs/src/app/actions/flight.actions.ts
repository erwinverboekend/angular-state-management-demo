export class GetFlights {
  static readonly type = '[FLIGHT] Get flights';
}

export class GetFlight {
  static readonly type = '[FLIGHT] Get flight';

  constructor(public payload: string) {}
}

export class DelayFlight {
  static readonly type = '[FLIGHT] Delay flight';

  constructor(public payload: string) {}
}

export class DeleteFlight {
  static readonly type = '[FLIGHT] Delete flight';

  constructor(public payload: string) {}
}
