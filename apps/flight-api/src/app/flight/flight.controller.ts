import { Controller, Delete, Get, HttpCode, HttpStatus, Param, Post, Query } from '@nestjs/common';
import { Flight } from '@nx-flights/data';
import { FlightService } from './flight.service';

@Controller('flights')
export class FlightController {

  constructor(private flightService: FlightService) {
  }

  @Get()
  searchFlights(@Query('query') query: string,
                @Query('delayed') delayed: string): Promise<Flight[]> {
    return this.flightService.search(query, delayed === '1');
  }

  @Get(':flightId')
  getFlight(@Param('flightId') flightId: string): Promise<Flight> {
    return this.flightService.get(flightId);
  }

  @HttpCode(HttpStatus.OK)
  @Post(':flightId/delay')
  delayFlight(@Param('flightId') flightId: string): Promise<void> {
    return this.flightService.delay(flightId);
  }

  @Delete(':flightId')
  deleteFlight(@Param('flightId') flightId: string): Promise<void> {
    return this.flightService.delete(flightId);
  }
}

