import { Injectable, NotFoundException } from '@nestjs/common';
import { Flight } from '@nx-flights/data';

const FLIGHTS: Flight[] = [
  {
    id: '1',
    from: 'Graz',
    to: 'Mallorca',
    date: '2020-03-19T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '2',
    from: 'Graz',
    to: 'Hamburg',
    date: '2020-03-16T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '3',
    from: 'Hamburg',
    to: 'Graz',
    date: '2020-03-16T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '4',
    from: 'Hamburg',
    to: 'Graz',
    date: '2020-03-16T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '5',
    from: 'Hamburg',
    to: 'Graz',
    date: '2020-03-16T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '6',
    from: 'Wien',
    to: 'Berlin',
    date: '2020-03-16T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '7',
    from: 'Wien',
    to: 'Berlin',
    date: '2020-03-16T13:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '8',
    from: 'Wien',
    to: 'Berlin',
    date: '2020-03-16T14:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '9',
    from: 'Wien',
    to: 'München',
    date: '2020-03-16T15:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '10',
    from: 'Wien',
    to: 'München',
    date: '2020-03-16T16:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '11',
    from: 'Wien',
    to: 'München',
    date: '2020-03-16T17:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '12',
    from: 'Wien',
    to: 'Zürich',
    date: '2020-03-16T18:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '13',
    from: 'Wien',
    to: 'Zürich',
    date: '2020-03-16T19:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '14',
    from: 'Wien',
    to: 'Zürich',
    date: '2020-03-16T20:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '15',
    from: 'Wien',
    to: 'Bern',
    date: '2020-03-16T21:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '16',
    from: 'Wien',
    to: 'Bern',
    date: '2020-03-16T22:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '17',
    from: 'Wien',
    to: 'Bern',
    date: '2020-03-16T23:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '18',
    from: 'Wien',
    to: 'Frankfurt',
    date: '2020-03-17T00:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '19',
    from: 'Wien',
    to: 'Frankfurt',
    date: '2020-03-17T01:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '20',
    from: 'Wien',
    to: 'Frankfurt',
    date: '2020-03-17T02:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '21',
    from: 'Wien',
    to: 'Salzburg',
    date: '2020-03-17T03:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '22',
    from: 'Wien',
    to: 'Salzburg',
    date: '2020-03-17T04:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '23',
    from: 'Wien',
    to: 'Salzburg',
    date: '2020-03-17T05:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '24',
    from: 'Wien',
    to: 'Stuttgart',
    date: '2020-03-17T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '25',
    from: 'Wien',
    to: 'Stuttgart',
    date: '2020-03-17T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '26',
    from: 'Wien',
    to: 'Stuttgart',
    date: '2020-03-17T08:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '27',
    from: 'Berlin',
    to: 'Wien',
    date: '2020-03-17T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '28',
    from: 'Berlin',
    to: 'Wien',
    date: '2020-03-17T10:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '29',
    from: 'Berlin',
    to: 'Wien',
    date: '2020-03-17T11:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '30',
    from: 'Berlin',
    to: 'München',
    date: '2020-03-17T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '31',
    from: 'Berlin',
    to: 'München',
    date: '2020-03-17T13:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '32',
    from: 'Berlin',
    to: 'München',
    date: '2020-03-17T14:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '33',
    from: 'Berlin',
    to: 'Zürich',
    date: '2020-03-17T15:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '34',
    from: 'Berlin',
    to: 'Zürich',
    date: '2020-03-17T16:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '35',
    from: 'Berlin',
    to: 'Zürich',
    date: '2020-03-17T17:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '36',
    from: 'Berlin',
    to: 'Bern',
    date: '2020-03-17T18:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '37',
    from: 'Berlin',
    to: 'Bern',
    date: '2020-03-17T19:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '38',
    from: 'Berlin',
    to: 'Bern',
    date: '2020-03-17T20:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '39',
    from: 'Berlin',
    to: 'Frankfurt',
    date: '2020-03-17T21:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '40',
    from: 'Berlin',
    to: 'Frankfurt',
    date: '2020-03-17T22:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '41',
    from: 'Berlin',
    to: 'Frankfurt',
    date: '2020-03-17T23:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '42',
    from: 'Berlin',
    to: 'Salzburg',
    date: '2020-03-18T00:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '43',
    from: 'Berlin',
    to: 'Salzburg',
    date: '2020-03-18T01:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '44',
    from: 'Berlin',
    to: 'Salzburg',
    date: '2020-03-18T02:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '45',
    from: 'Berlin',
    to: 'Stuttgart',
    date: '2020-03-18T03:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '46',
    from: 'Berlin',
    to: 'Stuttgart',
    date: '2020-03-18T04:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '47',
    from: 'Berlin',
    to: 'Stuttgart',
    date: '2020-03-18T05:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '48',
    from: 'München',
    to: 'Wien',
    date: '2020-03-18T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '49',
    from: 'München',
    to: 'Wien',
    date: '2020-03-18T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '50',
    from: 'München',
    to: 'Wien',
    date: '2020-03-18T08:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '51',
    from: 'München',
    to: 'Berlin',
    date: '2020-03-18T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '52',
    from: 'München',
    to: 'Berlin',
    date: '2020-03-18T10:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '53',
    from: 'München',
    to: 'Berlin',
    date: '2020-03-18T11:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '54',
    from: 'München',
    to: 'Zürich',
    date: '2020-03-18T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '55',
    from: 'München',
    to: 'Zürich',
    date: '2020-03-18T13:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '56',
    from: 'München',
    to: 'Zürich',
    date: '2020-03-18T14:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '57',
    from: 'München',
    to: 'Bern',
    date: '2020-03-18T15:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '58',
    from: 'München',
    to: 'Bern',
    date: '2020-03-18T16:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '59',
    from: 'München',
    to: 'Bern',
    date: '2020-03-18T17:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '60',
    from: 'München',
    to: 'Frankfurt',
    date: '2020-03-18T18:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '61',
    from: 'München',
    to: 'Frankfurt',
    date: '2020-03-18T19:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '62',
    from: 'München',
    to: 'Frankfurt',
    date: '2020-03-18T20:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '63',
    from: 'München',
    to: 'Salzburg',
    date: '2020-03-18T21:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '64',
    from: 'München',
    to: 'Salzburg',
    date: '2020-03-18T22:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '65',
    from: 'München',
    to: 'Salzburg',
    date: '2020-03-18T23:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '66',
    from: 'München',
    to: 'Stuttgart',
    date: '2020-03-19T00:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '67',
    from: 'München',
    to: 'Stuttgart',
    date: '2020-03-19T01:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '68',
    from: 'München',
    to: 'Stuttgart',
    date: '2020-03-19T02:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '69',
    from: 'Zürich',
    to: 'Wien',
    date: '2020-03-19T03:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '70',
    from: 'Zürich',
    to: 'Wien',
    date: '2020-03-19T04:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '71',
    from: 'Zürich',
    to: 'Wien',
    date: '2020-03-19T05:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '72',
    from: 'Zürich',
    to: 'Berlin',
    date: '2020-03-19T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '73',
    from: 'Zürich',
    to: 'Berlin',
    date: '2020-03-19T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '74',
    from: 'Zürich',
    to: 'Berlin',
    date: '2020-03-19T08:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '75',
    from: 'Zürich',
    to: 'München',
    date: '2020-03-19T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '76',
    from: 'Zürich',
    to: 'München',
    date: '2020-03-19T10:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '77',
    from: 'Zürich',
    to: 'München',
    date: '2020-03-19T11:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '78',
    from: 'Zürich',
    to: 'Bern',
    date: '2020-03-19T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '79',
    from: 'Zürich',
    to: 'Bern',
    date: '2020-03-19T13:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '80',
    from: 'Zürich',
    to: 'Bern',
    date: '2020-03-19T14:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '81',
    from: 'Zürich',
    to: 'Frankfurt',
    date: '2020-03-19T15:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '82',
    from: 'Zürich',
    to: 'Frankfurt',
    date: '2020-03-19T16:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '83',
    from: 'Zürich',
    to: 'Frankfurt',
    date: '2020-03-19T17:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '84',
    from: 'Zürich',
    to: 'Salzburg',
    date: '2020-03-19T18:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '85',
    from: 'Zürich',
    to: 'Salzburg',
    date: '2020-03-19T19:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '86',
    from: 'Zürich',
    to: 'Salzburg',
    date: '2020-03-19T20:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '87',
    from: 'Zürich',
    to: 'Stuttgart',
    date: '2020-03-19T21:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '88',
    from: 'Zürich',
    to: 'Stuttgart',
    date: '2020-03-19T22:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '89',
    from: 'Zürich',
    to: 'Stuttgart',
    date: '2020-03-19T23:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '90',
    from: 'Bern',
    to: 'Wien',
    date: '2020-03-20T00:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '91',
    from: 'Bern',
    to: 'Wien',
    date: '2020-03-20T01:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '92',
    from: 'Bern',
    to: 'Wien',
    date: '2020-03-20T02:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '93',
    from: 'Bern',
    to: 'Berlin',
    date: '2020-03-20T03:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '94',
    from: 'Bern',
    to: 'Berlin',
    date: '2020-03-20T04:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '95',
    from: 'Bern',
    to: 'Berlin',
    date: '2020-03-20T05:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '96',
    from: 'Bern',
    to: 'München',
    date: '2020-03-20T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '97',
    from: 'Bern',
    to: 'München',
    date: '2020-03-20T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '98',
    from: 'Bern',
    to: 'München',
    date: '2020-03-20T08:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '99',
    from: 'Bern',
    to: 'Zürich',
    date: '2020-03-20T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '100',
    from: 'Bern',
    to: 'Zürich',
    date: '2020-03-20T10:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '101',
    from: 'Bern',
    to: 'Zürich',
    date: '2020-03-20T11:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '102',
    from: 'Bern',
    to: 'Frankfurt',
    date: '2020-03-20T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '103',
    from: 'Bern',
    to: 'Frankfurt',
    date: '2020-03-20T13:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '104',
    from: 'Bern',
    to: 'Frankfurt',
    date: '2020-03-20T14:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '105',
    from: 'Bern',
    to: 'Salzburg',
    date: '2020-03-20T15:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '106',
    from: 'Bern',
    to: 'Salzburg',
    date: '2020-03-20T16:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '107',
    from: 'Bern',
    to: 'Salzburg',
    date: '2020-03-20T17:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '108',
    from: 'Bern',
    to: 'Stuttgart',
    date: '2020-03-20T18:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '109',
    from: 'Bern',
    to: 'Stuttgart',
    date: '2020-03-20T19:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '110',
    from: 'Bern',
    to: 'Stuttgart',
    date: '2020-03-20T20:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '111',
    from: 'Frankfurt',
    to: 'Wien',
    date: '2020-03-20T21:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '112',
    from: 'Frankfurt',
    to: 'Wien',
    date: '2020-03-20T22:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '113',
    from: 'Frankfurt',
    to: 'Wien',
    date: '2020-03-20T23:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '114',
    from: 'Frankfurt',
    to: 'Berlin',
    date: '2020-03-21T00:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '115',
    from: 'Frankfurt',
    to: 'Berlin',
    date: '2020-03-21T01:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '116',
    from: 'Frankfurt',
    to: 'Berlin',
    date: '2020-03-21T02:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '117',
    from: 'Frankfurt',
    to: 'München',
    date: '2020-03-21T03:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '118',
    from: 'Frankfurt',
    to: 'München',
    date: '2020-03-21T04:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '119',
    from: 'Frankfurt',
    to: 'München',
    date: '2020-03-21T05:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '120',
    from: 'Frankfurt',
    to: 'Zürich',
    date: '2020-03-21T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '121',
    from: 'Frankfurt',
    to: 'Zürich',
    date: '2020-03-21T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '122',
    from: 'Frankfurt',
    to: 'Zürich',
    date: '2020-03-21T08:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '123',
    from: 'Frankfurt',
    to: 'Bern',
    date: '2020-03-21T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '124',
    from: 'Frankfurt',
    to: 'Bern',
    date: '2020-03-21T10:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '125',
    from: 'Frankfurt',
    to: 'Bern',
    date: '2020-03-21T11:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '126',
    from: 'Frankfurt',
    to: 'Salzburg',
    date: '2020-03-21T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '127',
    from: 'Frankfurt',
    to: 'Salzburg',
    date: '2020-03-21T13:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '128',
    from: 'Frankfurt',
    to: 'Salzburg',
    date: '2020-03-21T14:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '129',
    from: 'Frankfurt',
    to: 'Stuttgart',
    date: '2020-03-21T15:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '130',
    from: 'Frankfurt',
    to: 'Stuttgart',
    date: '2020-03-21T16:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '131',
    from: 'Frankfurt',
    to: 'Stuttgart',
    date: '2020-03-21T17:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '132',
    from: 'Salzburg',
    to: 'Wien',
    date: '2020-03-21T18:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '133',
    from: 'Salzburg',
    to: 'Wien',
    date: '2020-03-21T19:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '134',
    from: 'Salzburg',
    to: 'Wien',
    date: '2020-03-21T20:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '135',
    from: 'Salzburg',
    to: 'Berlin',
    date: '2020-03-21T21:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '136',
    from: 'Salzburg',
    to: 'Berlin',
    date: '2020-03-21T22:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '137',
    from: 'Salzburg',
    to: 'Berlin',
    date: '2020-03-21T23:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '138',
    from: 'Salzburg',
    to: 'München',
    date: '2020-03-22T00:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '139',
    from: 'Salzburg',
    to: 'München',
    date: '2020-03-22T01:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '140',
    from: 'Salzburg',
    to: 'München',
    date: '2020-03-22T02:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '141',
    from: 'Salzburg',
    to: 'Zürich',
    date: '2020-03-22T03:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '142',
    from: 'Salzburg',
    to: 'Zürich',
    date: '2020-03-22T04:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '143',
    from: 'Salzburg',
    to: 'Zürich',
    date: '2020-03-22T05:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '144',
    from: 'Salzburg',
    to: 'Bern',
    date: '2020-03-22T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '145',
    from: 'Salzburg',
    to: 'Bern',
    date: '2020-03-22T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '146',
    from: 'Salzburg',
    to: 'Bern',
    date: '2020-03-22T08:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '147',
    from: 'Salzburg',
    to: 'Frankfurt',
    date: '2020-03-22T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '148',
    from: 'Salzburg',
    to: 'Frankfurt',
    date: '2020-03-22T10:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '149',
    from: 'Salzburg',
    to: 'Frankfurt',
    date: '2020-03-22T11:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '150',
    from: 'Salzburg',
    to: 'Stuttgart',
    date: '2020-03-22T12:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '151',
    from: 'Salzburg',
    to: 'Stuttgart',
    date: '2020-03-22T13:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '152',
    from: 'Salzburg',
    to: 'Stuttgart',
    date: '2020-03-22T14:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '153',
    from: 'Stuttgart',
    to: 'Wien',
    date: '2020-03-22T15:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '154',
    from: 'Stuttgart',
    to: 'Wien',
    date: '2020-03-22T16:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '155',
    from: 'Stuttgart',
    to: 'Wien',
    date: '2020-03-22T17:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '156',
    from: 'Stuttgart',
    to: 'Berlin',
    date: '2020-03-22T18:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '157',
    from: 'Stuttgart',
    to: 'Berlin',
    date: '2020-03-22T19:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '158',
    from: 'Stuttgart',
    to: 'Berlin',
    date: '2020-03-22T20:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '159',
    from: 'Stuttgart',
    to: 'München',
    date: '2020-03-22T21:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '160',
    from: 'Stuttgart',
    to: 'München',
    date: '2020-03-22T22:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '161',
    from: 'Stuttgart',
    to: 'München',
    date: '2020-03-22T23:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '162',
    from: 'Stuttgart',
    to: 'Zürich',
    date: '2020-03-23T00:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '163',
    from: 'Stuttgart',
    to: 'Zürich',
    date: '2020-03-23T01:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '164',
    from: 'Stuttgart',
    to: 'Zürich',
    date: '2020-03-23T02:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '165',
    from: 'Stuttgart',
    to: 'Bern',
    date: '2020-03-23T03:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '166',
    from: 'Stuttgart',
    to: 'Bern',
    date: '2020-03-23T04:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '167',
    from: 'Stuttgart',
    to: 'Bern',
    date: '2020-03-23T05:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '168',
    from: 'Stuttgart',
    to: 'Frankfurt',
    date: '2020-03-23T06:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '169',
    from: 'Stuttgart',
    to: 'Frankfurt',
    date: '2020-03-23T07:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '170',
    from: 'Stuttgart',
    to: 'Frankfurt',
    date: '2020-03-23T08:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '171',
    from: 'Stuttgart',
    to: 'Salzburg',
    date: '2020-03-23T09:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '172',
    from: 'Stuttgart',
    to: 'Salzburg',
    date: '2020-03-23T10:27:01.9466787+00:00',
    delayed: false
  },
  {
    id: '173',
    from: 'Stuttgart',
    to: 'Salzburg',
    date: '2020-03-23T11:27:01.9466787+00:00',
    delayed: false
  }
];

function resolveResponse<T>(data: T, delayMin?: number, delayMax?: number): Promise<T> {
  const delay = delayMax ? Math.random() * delayMax + delayMin : (delayMin ? delayMin : 0);
  return new Promise(resolve => setTimeout(() => resolve(data), delay));
}

@Injectable()
export class FlightService {

  search(query: string, delayed: boolean): Promise<Flight[]> {
    const flights = FLIGHTS.filter((flight: Flight) => {
      let include = true;

      if (query
        && flight.from.toLowerCase().indexOf(query.toLowerCase()) === -1
        && flight.to.toLowerCase().indexOf(query.toLowerCase()) === -1) {
        include = false;
      }

      if (flight.delayed !== delayed) {
        include = false;
      }

      return include;
    });

    return resolveResponse(flights, 500, 1500);
  }

  get(id: string): Promise<Flight> {
    const flight = FLIGHTS.find(flight => flight.id === id);

    if (flight) {
      return resolveResponse(flight, 500, 1500);
    } else {
      throw new NotFoundException();
    }
  }

  delay(id: string): Promise<void> {
    return resolveResponse(null);
  }

  delete(id: string): Promise<void> {
    const flight = FLIGHTS.find(flight => flight.id === id);

    if (flight) {
      return resolveResponse(null);
    } else {
      throw new NotFoundException();
    }
  }

}
