import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { FlightModule } from './features/flight/flight.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    AkitaNgRouterStoreModule,
    environment.production ? [] : AkitaNgDevtools,

    FlightModule,

    RouterModule.forRoot([{ path: '', redirectTo: 'flights', pathMatch: 'full' }])
  ],
  providers: [
    {
      provide: NG_ENTITY_SERVICE_CONFIG,
      useValue: {
        baseUrl: 'http://localhost:4200/api'
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
