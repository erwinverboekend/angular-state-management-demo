import { Injectable } from '@angular/core';
import { NgEntityService } from '@datorama/akita-ng-entity-service';
import { Flight } from '@nx-flights/data';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { FlightState, FlightStore } from '../state/flight.store';

@Injectable({ providedIn: 'root' })
export class FlightService extends NgEntityService<FlightState> {

  constructor(protected store: FlightStore) {
    super(store);
  }

  loadFlight(id: string): void {
    console.info('Load flight');

    this.get<Flight>(id).subscribe();
  }

  loadFlights(): void {
    console.info('Load flights');

    this.get<Flight[]>().subscribe();
  }

  delayFlight(id: string): Observable<void> {
    console.info('Delay flight');

    const endpoint = `${this.baseUrl}/${this.resourceName}/${id}/delay`;

    return this.getHttp().post<void>(endpoint, null).pipe(
      tap(() => this.store.update(id, { delayed: true }))
    );
  }

  deleteFlight(id: string): Observable<void> {
    console.info('Delete flight');

    return this.delete(id);
  }

}
