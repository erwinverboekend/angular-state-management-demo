import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { RouterQuery } from '@datorama/akita-ng-router-store';
import { switchMap } from 'rxjs/operators';

import { FlightState, FlightStore } from './flight.store';

@Injectable({ providedIn: 'root' })
export class FlightQuery extends QueryEntity<FlightState> {

  selectedFlight$ = this.routerQuery.selectParams('flightId').pipe(
    switchMap((flightId: string) => this.selectEntity(flightId))
  );

  constructor(protected store: FlightStore,
              private routerQuery: RouterQuery) {
    super(store);
  }

}
