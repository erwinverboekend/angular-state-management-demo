import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

import { Flight } from '@nx-flights/data';

export interface FlightState extends EntityState<Flight> {}

@StoreConfig({ name: 'flights' })
@Injectable({ providedIn: 'root' })
export class FlightStore extends EntityStore<FlightState> {

  constructor() {
    super();
  }

}

