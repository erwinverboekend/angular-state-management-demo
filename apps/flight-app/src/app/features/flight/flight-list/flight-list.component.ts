import { Component, OnInit } from '@angular/core';
import { NgEntityServiceLoader } from '@datorama/akita-ng-entity-service';
import { startWith } from 'rxjs/operators';
import { FlightService } from '../../../services/flight.service';
import { FlightQuery } from '../../../state/flight.query';

@Component({
  selector: 'nx-flight-list',
  templateUrl: './flight-list.component.html',
  styleUrls: ['./flight-list.component.scss']
})
export class FlightListComponent implements OnInit {

  flights$ = this.flightQuery.selectAll();
  flightCount$ = this.flightQuery.selectCount();
  flightsLoading$ = this.loader.loadersFor('flights').get$.pipe(startWith(!this.flightQuery.getHasCache()));

  constructor(
    private flightQuery: FlightQuery,
    private flightService: FlightService,
    private loader: NgEntityServiceLoader) {
  }

  ngOnInit(): void {
    // Are there already flights in the store?
    const hasCache = this.flightQuery.getHasCache();

    if (!hasCache) {
      // Load flights if there are no flights in the store
      this.flightService.loadFlights();
    }
  }

}
