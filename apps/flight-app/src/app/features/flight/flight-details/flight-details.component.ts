import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterQuery } from '@datorama/akita-ng-router-store';
import { FlightService } from '../../../services/flight.service';

import { FlightQuery } from '../../../state/flight.query';

@Component({
  selector: 'nx-flight-details',
  templateUrl: './flight-details.component.html'
})
export class FlightDetailsComponent implements OnInit {

  flightId: string;
  flight$ = this.flightQuery.selectedFlight$;

  constructor(private location: Location,
              private routerQuery: RouterQuery,
              private flightQuery: FlightQuery,
              private flightService: FlightService) {
  }

  ngOnInit(): void {
    this.flightId = this.routerQuery.getParams<string>('flightId');

    // Load flight if it is not available in the store yet
    if (!this.flightQuery.hasEntity(this.flightId)) {
      this.flightService.loadFlight(this.flightId);
    }
  }

  delayFlight(): void {
    this.flightService.delayFlight(this.flightId).subscribe(() => {
      this.location.back();
    });
  }

  deleteFlight(): void {
    this.flightService.deleteFlight(this.flightId).subscribe(() => {
      this.location.back();
    });
  }

}
