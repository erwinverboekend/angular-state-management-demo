import { FlightDetailsComponent } from './flight-details/flight-details.component';
import { FlightListComponent } from './flight-list/flight-list.component';

export const FLIGHT_ROUTES = [
  {
    path: 'flights',
    component: FlightListComponent
  },
  {
    path: 'flights/:flightId',
    component: FlightDetailsComponent
  }
];
