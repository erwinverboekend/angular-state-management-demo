export interface Flight {
  readonly id: string;
  readonly from: string;
  readonly to: string;
  readonly date: string;
  readonly delayed: boolean;
}
